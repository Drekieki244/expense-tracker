import React, { useEffect, useState } from "react";
import ExpensesTable, {
  ExpensesCategoriesProps,
  ExpensesDetailsProps,
} from "./Components/ExpensesTable";
import AddExpenseForm from "./Components/AddExpenseForm";
import Balance from "./Components/Balance";

const App: React.FC = () => {
  const expensesDummyData: ExpensesCategoriesProps[] = [
    {
      id: "firstExpenseId",
      categoryName: "category 1",
      expenseDetails: [
        {
          id: "firstExpenseId1",
          expenseName: "expense1.1",
          amount: 300,
        },
        {
          id: "firstExpenseId2",
          expenseName: "expense1.2",
          amount: 300,
        },
        {
          id: "firstExpenseId3",
          expenseName: "expense1.3",
          amount: 300,
        },
      ],
    },
    {
      id: "secondExpenseId",
      categoryName: "category 2",
      expenseDetails: [
        {
          id: "secondExpenseId1",
          expenseName: "expense2.1",
          amount: 300,
        },
        {
          id: "secondExpenseId2",
          expenseName: "expense2.2",
          amount: 300,
        },
        {
          id: "secondExpenseId3",
          expenseName: "expense2.3",
          amount: 300,
        },
      ],
    },
    {
      id: "thirdExpenseId",
      categoryName: "category 3",
      expenseDetails: [
        {
          id: "thirdExpenseId1",
          expenseName: "expense3.1",
          amount: 300,
        },
        {
          id: "thirdExpenseId2",
          expenseName: "expense3.2",
          amount: 300,
        },
        {
          id: "thirdExpenseId3",
          expenseName: "expense3.3",
          amount: 300,
        },
      ],
    },
  ];
  const [expenses, setExpenses] =
    useState<ExpensesCategoriesProps[]>(expensesDummyData);
  const categoryOptions = expenses.map((expense) => ({
    id: expense.id,
    name: expense.categoryName,
  }));
  const calculateTotalAmount = (expenseDetails: ExpensesDetailsProps[]) => {
    return expenseDetails.reduce((total, expense) => total + expense.amount, 0);
  };

  const totalAmount = expenses.reduce(
    (total, category) => total + calculateTotalAmount(category.expenseDetails),
    0
  );
  const myWallet = 3000;
  const [spentTotal, setSpentTotal] = useState<number>(totalAmount);
  useEffect(() => {
    const calculateTotalAmount = (expenseDetails: ExpensesDetailsProps[]) => {
      return expenseDetails.reduce(
        (total, expense) => total + expense.amount,
        0
      );
    };

    const totalAmount = expenses.reduce(
      (total, category) =>
        total + calculateTotalAmount(category.expenseDetails),
      0
    );
    setSpentTotal(totalAmount);
  }, [expenses]);
  return (
    <div className="flex justify-center">
      <div className="flex flex-col space-y-6">
        <Balance spentAmount={spentTotal} walletAmount={myWallet} />
        <ExpensesTable expenses={expenses} />
        <AddExpenseForm
          expenses={expenses}
          categoryOptions={categoryOptions}
          setExpenses={setExpenses}
        />
      </div>
    </div>
  );
};

export default App;
