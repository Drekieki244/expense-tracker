import React from "react";
import { FormData } from "./useAddExpense";
import { ExpensesCategoriesProps } from "../ExpensesTable";
import { useForm } from "react-hook-form";
export interface AddExpenseFormProps {
  expenses: ExpensesCategoriesProps[];
  categoryOptions: {
    name: string;
    id: string;
  }[];
  setExpenses: React.Dispatch<React.SetStateAction<ExpensesCategoriesProps[]>>;
}

const AddExpenseForm: React.FC<AddExpenseFormProps> = ({
  expenses,
  categoryOptions,
  setExpenses,
}) => {
  const onSubmit = (data: FormData) => {
    const { categoryId, expenseName, amount } = data;
    setExpenses((prevExpenses) =>
      prevExpenses.map((category) => {
        if (category.id === categoryId) {
          const newExpense = {
            id: `newExpenseId${Date.now()}`,
            expenseName,
            amount: Number(amount),
          };
          return {
            ...category,
            expenseDetails: [...category.expenseDetails, newExpense],
          };
        } else {
          return category;
        }
      })
    );
  };

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FormData>();

  const onSubmitHandler = (data: FormData) => {
    onSubmit(data);
  };
  return (
    <>
      <form onSubmit={handleSubmit(onSubmitHandler)} className="flex space-x-4">
        <div className="flex flex-col space-x-2">
          <label className="text-white" htmlFor="categoryId">
            Category
          </label>
          <select
            {...register("categoryId")}
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
          >
            {categoryOptions.map((option) => (
              <option key={option.id} value={option.id}>
                {option.name}
              </option>
            ))}
          </select>
        </div>
        <div className="flex flex-col space-x-2">
          <label className="text-white" htmlFor="expenseName">
            Expense Name
          </label>
          <input
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            type="text"
            id="expenseName"
            {...register("expenseName", { required: true })}
          />
          {errors.expenseName && <span>This field is required</span>}
        </div>
        <div className="flex flex-col space-x-2">
          <label className="text-white" htmlFor="amount">
            Amount
          </label>
          <input
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            type="number"
            id="amount"
            {...register("amount", { required: true })}
          />
          {errors.amount && <span>This field is required</span>}
        </div>
        <button
          className="rounded-full bg-slate-400 px-4"
          style={{ color: "white" }}
          type="submit"
        >
          Add Expense
        </button>
      </form>
    </>
  );
};

export default AddExpenseForm;
