import { useForm } from "react-hook-form";
import { ExpensesCategoriesProps } from "../ExpensesTable";
export interface AddExpenseFormProps {
  categories: ExpensesCategoriesProps[];
  onSubmit: (data: FormData) => void;
}
export interface FormData {
  categoryId: string;
  expenseName: string;
  amount: number;
}
const useAddExpense = ({ categories, onSubmit }: AddExpenseFormProps) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FormData>();

  const onSubmitHandler = (data: FormData) => {
    onSubmit(data);
  };
  return { register, handleSubmit, errors, onSubmitHandler };
};
export default useAddExpense;
