import React from "react";

export interface ExpensesDetailsProps {
  id: string;
  expenseName: string;
  amount: number;
}
export interface ExpensesCategoriesProps {
  id: string;
  categoryName: string;
  expenseDetails: ExpensesDetailsProps[];
}
export interface ExpensesTableProps {
  expenses: ExpensesCategoriesProps[];
}

const ExpensesTable: React.FC<ExpensesTableProps> = ({ expenses }) => {
  const calculateTotalAmount = (expenseDetails: ExpensesDetailsProps[]) => {
    return expenseDetails.reduce((total, expense) => total + expense.amount, 0);
  };

  const totalAmount = expenses.reduce(
    (total, category) => total + calculateTotalAmount(category.expenseDetails),
    0
  );
  return (
    <div style={{ width: "fit-content" }}>
      <div
        className="flex flex-row border-2 m-2 bg-slate-100"
        style={{ borderRadius: "20px" }}
      >
        {expenses.map((expense, idx) => (
          <div
            className={`flex flex-col p-4 ${
              expenses.length - 1 !== idx && "border-r-2"
            }`}
            key={expense.id}
          >
            <div className="flex justify-center">
              <h1>{expense.categoryName}</h1>
            </div>
            <div>
              <div
                key={`expenses titles - ${expense.id}`}
                className="flex flex-row"
              >
                <div className="p-1">
                  <h2>Expense Name</h2>
                </div>
                <div className="p-1">
                  <h2>Amount</h2>
                </div>
              </div>
              {expense.expenseDetails.map((expenseDetails) => (
                <div
                  key={expenseDetails.id}
                  className="flex flex-row justify-between"
                >
                  <div className="p-1">
                    <h2>{expenseDetails.expenseName}</h2>
                  </div>
                  <div className="p-1">
                    <h2>{expenseDetails.amount}</h2>
                  </div>
                </div>
              ))}
            </div>
          </div>
        ))}
      </div>
      <div className="flex justify-end">
        <h4 className="text-white">Total= </h4>
        <h4 className="text-white">{totalAmount}</h4>
      </div>
    </div>
  );
};

export default ExpensesTable;
