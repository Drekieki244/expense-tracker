import React from "react";
export interface BalanceProps {
  spentAmount: number;
  walletAmount: number;
}

const Balance: React.FC<BalanceProps> = ({ spentAmount, walletAmount }) => {
  const rest = walletAmount - spentAmount;
  return (
    <div className="pt-10">
      <div
        className="flex justify-center border-2 m-2 bg-slate-100 py-20"
        style={{ borderRadius: "20px" }}
      >
        <div className="flex justify-center space-x-2">
          <div className="flex flex-col border-r-2 px-4">
            <div>
              <h1 className="text-center">My Wallet</h1>
            </div>
            <div>
              <h2 className="text-center">{walletAmount}</h2>
            </div>
          </div>
          <div>
            <div>
              <h1 className="text-center">Expences</h1>
            </div>
            <div>
              <h2 className="text-center">{spentAmount}</h2>
            </div>
          </div>
        </div>
      </div>
      <div className="flex justify-end space-x-2">
        <h1 className="text-white">Rest: </h1>
        <h1 className={rest > 0 ? "text-green-500" : "text-red-500"}>{rest}</h1>
      </div>
    </div>
  );
};

export default Balance;
